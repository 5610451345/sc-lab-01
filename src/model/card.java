package model;

public class card {
	private int balance;
	private String name;
	
	public card(int amount) {
		this.balance = amount;
	}
	
	public void deposit(int amount) {
		balance += amount;
	}
	
	public void withdraw(int amount) {
		balance -= amount;
	}
	
	public void setName(String nam) {
		name = nam;
	}
	
	public String toString() {
		return balance + " Baht";
	}

}
