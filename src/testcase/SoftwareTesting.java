package testcase;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import gui.SoftwareFrame;
import model.card;


public class SoftwareTesting {

    class ListenerMgr implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				System.exit(0);
				
			}
			   
		   }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
         new SoftwareTesting();
	}
	public SoftwareTesting() {
		frame = new SoftwareFrame();
        frame.pack();
        frame.setVisible(true);
        frame.setSize(400,400);
        list = new ListenerMgr();
        frame.setListener(list);
        setTestCase();
	}
    public void setTestCase() {
    	/* Student may modify this code and write your result here 
        frame.setResult("aaa\t bbb\t ccc\t");
        frame.extendResult("ddd");  */
    	card ca = new card(0);
		frame.setResult("Balance : " + ca.toString());
		frame.extendResult("Deposit : " + 500);
		ca.deposit(500);
		frame.extendResult("Balance : " + ca.toString());
		frame.extendResult("Withdraw : " + 50);
		ca.withdraw(50);
		frame.extendResult("Balance : " + ca.toString());
    	
    }
    ActionListener list;
    SoftwareFrame frame;
}
